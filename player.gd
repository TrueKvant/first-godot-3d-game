extends CharacterBody3D

signal hit

# In Godot 4.0 @export replaces export keyword
# There are many other keywords being replace by anotations.
# Anotations are 'keywords' starting with '@'.
# see: https://docs.godotengine.org/en/stable/tutorials/scripting/gdscript/gdscript_exports.html
# export anotation enables editing its value in the editor
@export var speed = 14.0
@export var jump_impulse = 20.0
@export var fall_acceleration = 75.0

@export var bounce_impulse = 16.0

# velocity is not needed in GD 4.0 since it is already part of the parent class (CharacterBody3D)
#var velocity = Vector3.ZERO

# init the up_direction
func _init():
	up_direction = Vector3.UP

# should be used for the physics object (physics body or kinematics body)
# kinda fixed_update from Unity3D
func _physics_process(delta):
	var direction = Vector3.ZERO
	if Input.is_action_pressed("move_right"):
		direction.x += 1
	if Input.is_action_pressed("move_left"):
		direction.x -= 1
		
	if Input.is_action_pressed("move_back"):
		direction.z += 1
	if Input.is_action_pressed("move_forward"):
		direction.z -= 1
	
	# normalization
	if direction != Vector3.ZERO:
		direction = direction.normalized()
		$Pivot.look_at(position + direction, Vector3.UP)
		#               '-> in Godot 4.0 the variable is called 'position'
		# New in Godot 4.0: playback_speed in now speed_scale
		$AnimationPlayer.speed_scale = 4.0
	else:
		# New in Godot 4.0: playback_speed in now speed_scale
		$AnimationPlayer.speed_scale = 1.0
	
	velocity.x = direction.x * speed
	velocity.z = direction.z * speed
	
	if is_on_floor() and Input.is_action_pressed("jump"):
		velocity.y += jump_impulse
	
	# apply gravity
	velocity.y -= fall_acceleration*delta #if not is_on_floor() else 0
	
	# compute physics including the sliding
	# in GD 4.0 is calcilated using the velocity variable
	move_and_slide()
	
	var count = get_slide_collision_count()
	for index in count:
		var collision = get_slide_collision(index)
		if collision.get_collider().is_in_group("mob"):
			var mob = collision.get_collider()
			# check for kinda upward direction
			# mob has box collider, so it should work
			if Vector3.UP.dot(collision.get_normal()) > 0.1:
				mob.squash()
				velocity.y = bounce_impulse
	
	# making model follows the jumping curve
	$Pivot.rotation.x = PI / 6 * velocity.y / jump_impulse

func die():
	emit_signal("hit")
	queue_free()

func _on_mob_detector_body_entered(_body):
	die()
