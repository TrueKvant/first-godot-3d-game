extends CharacterBody3D

signal squashed

@export var min_speed = 10.0
@export var max_speed = 18.0

# velocity is not needed in GD 4.0 since it is already part of the parent class (CharacterBody3D)
#var velocity = Vector3.ZERO

# use underscroll if we are intentionally not using the argument delta
func _physics_process(_delta):
	move_and_slide()

func initialize(start_position, player_position):
	position = start_position
	# '-> position replaces translation in Godot 4.0
	look_at_from_position(position, player_position, Vector3.UP)
	# '-> look_at_from_position is used since the mob is not in the scene tree yet
	# rotate somewhat close to the player
	rotate_y(randf_range(-PI/4.0, PI/4.0))
	
	var random_speed = randf_range(min_speed, max_speed)
	
	velocity = Vector3.FORWARD * random_speed
	velocity = velocity.rotated(Vector3.UP, rotation.y)
	
	# New in Godot 4.0: playback_speed in now speed_scale
	$AnimationPlayer.speed_scale = random_speed / min_speed


func squash():
	emit_signal("squashed")
	queue_free()
	
	
func _on_visible_on_screen_notifier_3d_screen_exited():
	# free it this from existence
	queue_free()
