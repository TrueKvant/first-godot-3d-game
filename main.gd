extends Node

# preload loads the scene when compile
# load loads the scene on demand (e.g. in side the function)
# New in Godot 4.0: the types are hinted similarly to the typed python
@export var mob_scene: PackedScene = preload("res://mob.tscn")

func _ready():
	randomize()
	
func _unhandled_input(event):
	if event.is_action_pressed("ui_accept") and $UserInterface/Retry.visible:
		get_tree().reload_current_scene()

func _on_mob_timer_timeout():
	
	# create new mob instance
	var mob = mob_scene.instantiate()
	
	var mob_spawn_location = $SpawnPath/SpawnLocation
	# set progress_ratio to random position
	# New in Godot 4.0: unit offset is now called progress_ratio
	mob_spawn_location.progress_ratio = randf()
	
	var player_position = $Player.position
	
	mob.initialize(mob_spawn_location.position, player_position)
	
	mob.connect("squashed", $UserInterface/ScoreLabel._on_mob_squashed)
	
	# add new mob as child of main node
	# new in Godot 4: this should be the last thing you done to ensure the position and velocity
	#                 have been set correctly (Thank you Goalatio from Youtube comments)
	add_child(mob)
	
	
func _on_player_hit():
	$MobTimer.stop()
	$UserInterface/Retry.show()
